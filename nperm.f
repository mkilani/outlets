      PROGRAM PARTITION
      IMPLICIT NONE
      INTEGER NMAX
      PARAMETER (NMAX = 1000)
      INTEGER N, NP, PART (NMAX, NMAX)
      INTEGER PATTERNS(NMAX,NMAX)
      CHARACTER (LEN = 10) STR 
      INTEGER IARG
      INTEGER I, J
      CHARACTER (LEN=5) STRN
      
      CALL GETARG(1,STR)
      READ(STR,*) N
      
      CALL AUP ( N , PART, NP)


      WRITE(STRN,'(I5)') N

      DO 1, I=1,NP
  1   PRINT "(I5,A3,"//STRN//"I5)", NP-I+1, " : ",(PART(I,J), J=1,N)


      PRINT*
      WRITE (6,6) N, NP 
  6   FORMAT("Number of unique partitions of ", I5, " is ", I5)



      END 


      SUBROUTINE AUP ( N , PART, NP )
      IMPLICIT NONE
      INTEGER NMAX
      PARAMETER (NMAX = 1000)
      INTEGER P (NMAX)
      INTEGER N, NP
      INTEGER PART(NMAX,NMAX)
      INTEGER K, I
      INTEGER R
      INTEGER IT

      NP = 0

      K = 1

      P(K) = N

 1    CONTINUE

      NP = NP + 1

      DO 5, I=1,K
 5    PART(NP,I) = P(I)

C     PRINT*,  (P(I), I=1,K)

      R = 0

 2    CONTINUE
      IF ( K .GE. 1 .AND. P(K).EQ.1 ) THEN
        R = R + P(K)
        K = K - 1
        GOTO 2
      ENDIF

      IF ( K .LT. 1 ) GOTO 10 


      P(K) = P(K) - 1
      R = R + 1


 3    CONTINUE
      IF ( R.GT.P(K) ) THEN
        P(K+1) = P(K)
        R = R - P(K)
        K = K + 1
        GOTO 3
      ENDIF

      P(K+1) = R
      K = K + 1

C     PRINT* , K, R

      GOTO 1
 10   CONTINUE

      RETURN
      END



