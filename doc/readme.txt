(npartition.f) "./npart n"   ->   unique partition of integer n
(outlets.f) "./outlets n m p" ->  generate non-isomorphic graphs for n firms with m outlets each (population is p)
